# Bowling Game Kata TDD Example

## Support

This tutorial is released into the public domain by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/)  under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) for further details.